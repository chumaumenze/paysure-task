import traceback

from flask import request, Blueprint
from flask_validator import ValidatorEngine
from psycopg2.errors import UniqueViolation
from sqlalchemy.exc import IntegrityError

from errors import PolicyAlreadyExist, APIError, InvalidRequestData
from util import handle_response, create_policy, create_payment
from util import parse_payment_xml

api = Blueprint('api', __name__)
validator = ValidatorEngine()

POLICY_SCHEMA = {
    'external_user_id': [r'regex:\w+', 'required', 'max:64'],
    'benefit': ['alpha', 'required', 'max:64'],
    'currency': ['alpha', 'required', 'max:3'],
    'total_max_amount': ['integer', 'required']
}

PAYMENT_SCHEMA = {
    'external_user_id': [r'regex:\w+', 'required', 'max:64'],
    'benefit': ['alpha', 'required', 'max:64'],
    'currency': ['alpha', 'required', 'max:3'],
    'amount': ['integer', 'required'],
    'timestamp': [r'regex:^[\d\+-T:]+$', 'required']
}


@api.route('/')
def hello_world():
    return handle_response(message='Hello World!')


@api.route('/payment', methods=['POST'])
def make_payment():
    if request.content_type == 'application/json':
        payment_data = request.get_json()
    elif request.content_type == 'application/xml':
        xml_data = request.data.decode('utf-8')
        payment_data = parse_payment_xml(xml_data)
    else:
        raise InvalidRequestData

    if not validator.validate(payment_data, PAYMENT_SCHEMA):
        return validator.errors.response()

    response_body = {'authorized': True, 'reason': None}
    try:
        create_payment(payment_data.get('external_user_id'),
                       payment_data.get('benefit'),
                       payment_data.get('currency'),
                       payment_data.get('amount'),
                       payment_data.get('timestamp'))
    except APIError as e:
        response_body['authorized'] = False
        response_body['reason'] = e.message

    return handle_response(body=response_body, code=201)


@api.route('/policy', methods=['POST'])
@validator('json', POLICY_SCHEMA)
def upload_policy():
    policy_data = request.get_json()
    try:
        create_policy(policy_data.get('external_user_id'),
                      policy_data.get('benefit'),
                      policy_data.get('currency'),
                      policy_data.get('total_max_amount'))
    except IntegrityError as e:
        traceback.print_exc()
        if isinstance(e.orig, UniqueViolation):
            raise PolicyAlreadyExist
        raise

    return handle_response(code=201)
