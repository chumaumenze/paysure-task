from flask import Flask
from sqlalchemy.exc import SQLAlchemyError
from werkzeug.exceptions import HTTPException

from models import db
from routes import api
from util import APIError, handle_exception, register_exception_handler

app = Flask(__name__)
app.config.from_pyfile('config.py')

app.register_blueprint(api)
db.init_app(app)
register_exception_handler(app, handle_exception, APIError,
                           HTTPException, SQLAlchemyError)

with app.app_context():
    db.Model.metadata.reflect(db.engine)  # load existing DB schema
    db.create_all()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
