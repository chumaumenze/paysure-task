## PaySure Insurance

#### Requirements

+ Python 3.7+
+ Pipenv

#### Run the Server

Provide database URI in [config.py](./config.py). Then execute the following:

```bash
pipenv install --ignore-pipfile
python app.py
```

API is accessible at `https://localhost:8000`


##### Endpoints


+ `/`: Test API Connectivity
+ `/payment`
    + Process payments.
    + Requires JSON/XML payload via `POST`.
    + Headers must define `Content-Type` to either `application/json` or `application/xml`.
    ##### JSON Sample
    ```json
    { 
        "external_user_id": "user1",
        "benefit": "dentist",
        "currency": "GBP",
        "amount": 42,
        "timestamp": "2019-01-08T22:51:13+00:00"
    }
    ```
    ##### XML Sample
    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <root>
       <Bill_Amt>42</Bill_Amt>
       <MCC_Desc>dentist</MCC_Desc>
       <Txn_Ctry>GBP</Txn_Ctry>
       <Token>user1</Token>
       <TXN_Time_DE>20190108225113</TXN_Time_DE>
    </root>
    ```
+ `/policy`
    + Upload policy.
    + Requires JSON/XML payload via `POST`.
    + Headers must define `Content-Type` to either `application/json` or `text/xml`.
    ```json
    {
        "external_user_id": "user1",
        "benefit": "dentist",
        "total_max_amount": 1000,
        "currency": "gbp"
    }
    ```

#### Testing

```bash
nose2 --with-coverage tests
```