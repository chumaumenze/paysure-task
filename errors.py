class APIError(Exception):
    """
    Base API error class
    """
    code = 500
    message = 'Internal server error'

    def __init__(self, message=None, code=None):
        self.message = message or getattr(self.__class__, 'message', None)
        self.code = code or getattr(self.__class__, 'code', None)


class PolicyNotFound(APIError):
    code = 200
    message = 'POLICY_NOT_FOUND'


class PolicyAmountExceeded(APIError):
    code = 200
    message = 'POLICY_AMOUNT_EXCEEDED'


class PolicyAlreadyExist(APIError):
    code = 200
    message = 'POLICY_ALREADY_EXIST'


class InvalidRequestData(APIError):
    code = 400
    message = 'INVALID_REQUEST_DATA'
