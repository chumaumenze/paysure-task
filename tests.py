import unittest

from contextlib import contextmanager

from app import app
from models import Policy, Payment
from util import handle_response, handle_exception, APIError


class TestUtil(unittest.TestCase):

    def test_handle_response(self):
        expected_result = {
            'status': 'SUCCESS',
            'message': 'This is a test message'
        }, 200

        message = 'This is a test message'
        result = handle_response(message)
        self.assertEqual(expected_result, result)

    def test_handle_exception_failure(self):
        expected_result = {
            'status': 'FAILURE',
            'message': 'This is a test failure message'
        }, 400

        exception = APIError('This is a test failure message', code=400)
        result = handle_exception(exception)
        self.assertEqual(expected_result, result)

    def test_handle_exception_error(self):
        expected_result = {
            'status': 'ERROR',
            'message': 'This is a test error message'
        }, 500

        exception = APIError('This is a test error message')
        result = handle_exception(exception)
        self.assertEqual(expected_result, result)

    def test_handle_builtin_exception(self):
        expected_result = {
            'status': 'ERROR',
            'message': 'Internal server error'
        }, 500

        exception = ValueError()
        result = handle_exception(exception)
        self.assertEqual(expected_result, result)


class TestAPIError(unittest.TestCase):

    def test_api_error(self):
        message = 'Test message'
        code = 800
        self.api_error = APIError(message, code)
        self.assertEqual(self.api_error.message, message)
        self.assertEqual(self.api_error.code, code)

    def test_api_error_without_args(self):
        message = 'Internal server error'
        code = 500
        self.api_error = APIError()
        self.assertEqual(self.api_error.message, message)
        self.assertEqual(self.api_error.code, code)


class TestApp(unittest.TestCase):

    def setUp(self) -> None:
        self.app = app.test_client()
        self.payment_json = {
            "external_user_id": "user1",
            "benefit": "dentist",
            "currency": "GBP",
            "amount": 42,
            "timestamp": "2019-01-08T22:51:13+00:00"
        }
        self.policy_json = {
            "external_user_id": "user1",
            "benefit": "dentist",
            "total_max_amount": 1000,
            "currency": "gbp"
        }

    @contextmanager
    def add_stub_data(self):
        with app.app_context():
            self.policy = Policy(**self.policy_json).save()
            self.payment = Payment(**self.payment_json).save()
            yield
            self.policy.delete()
            self.payment.delete()

    def test_hello_world(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(200, response.status_code)
        self.assertIn(b'Hello World!', response.data)


#TODO: Add more real tests