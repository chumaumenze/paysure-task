import datetime

import untangle

from sqlalchemy import func

from errors import APIError, PolicyAmountExceeded, PolicyNotFound
from errors import InvalidRequestData
from models import db, Policy, Payment


def create_policy(user_id, benefit, currency, max_amount):
    user_id = user_id.lower()
    benefit = benefit.lower()
    currency = currency.lower()
    policy = Policy(
        external_user_id=user_id, benefit=benefit,
        currency=currency, total_max_amount=max_amount)
    return policy.save()


def create_payment(user_id, benefit, currency, amount, timestamp):
    timestamp = datetime.datetime.fromisoformat(timestamp)
    user_id = user_id.lower()
    benefit = benefit.lower()
    currency = currency.lower()
    user_policy = Policy.get(external_user_id=user_id, benefit=benefit,
                             currency=currency)
    if user_policy is None:
        raise PolicyNotFound

    sum_prev_payments = db.session.query(
        func.sum(Payment.amount).label("total_amount")
    ).filter_by(
        external_user_id=user_id, benefit=benefit, currency=currency
    ).group_by(Payment.external_user_id).first()

    if sum_prev_payments is not None:
        total_payments = sum_prev_payments.total_amount + amount
        if total_payments > user_policy.total_max_amount:
            raise PolicyAmountExceeded

    new_payment = Payment(
        external_user_id=user_id.lower(), benefit=benefit.lower(),
        currency=currency.lower(), amount=amount, timestamp=timestamp)
    return new_payment.save()


def register_exception_handler(app, handler_func, /, *exceptions):
    for exc in exceptions:
        app.register_error_handler(exc, handler_func)


def handle_response(message=None, body=None, status='success', code=200):
    return body or {
        'status': status.upper(),
        'message': message
    }, code


def handle_exception(exception):
    code = getattr(exception, 'code', 500)
    message = getattr(
        exception, 'description',
        getattr(exception, 'message', APIError.message)
    )

    exception_type = 'ERROR' if str(code)[:1] == '5' else 'FAILURE'

    return handle_response(message=message, status=exception_type,
                           code=code)


def parse_payment_xml(xml):
    try:
        parsed_xml = untangle.parse(xml.strip())
        timestamp = parsed_xml.root.TXN_Time_DE.cdata.strip()
        timestamp = datetime.datetime.strptime(timestamp, '%Y%m%d%H%M%S')
        return {
            'amount': int(parsed_xml.root.Bill_Amt.cdata.strip()),
            'benefit': parsed_xml.root.MCC_Desc.cdata.strip(),
            'currency': parsed_xml.root.Txn_Ctry.cdata.strip(),
            'external_user_id': parsed_xml.root.Token.cdata.strip(),
            'timestamp': timestamp.isoformat()
        }
    except:
        raise InvalidRequestData
