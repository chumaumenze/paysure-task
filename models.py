import datetime
import traceback

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import SQLAlchemyError, DBAPIError

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    external_user_id = db.Column(db.String(64), index=True,
                                 nullable=False)
    benefit = db.Column(db.String(64), nullable=False)
    currency = db.Column(db.String(3), nullable=False)

    @staticmethod
    def safe_commit(db_object):
        try:
            db.session.commit()
        except (SQLAlchemyError, DBAPIError):
            db.session.rollback()
            traceback.print_exc()
            raise

    @classmethod
    def get(cls, **kwargs):
        return cls.query.filter_by(**kwargs).first()

    def save(self, ):
        db.session.add(self)
        self.safe_commit(self)
        return self

    def delete(self):
        db.session.delete(self)
        self.safe_commit(self)


class Policy(BaseModel):
    __tablename__ = 'policies'
    total_max_amount = db.Column(db.Integer, nullable=False)
    __table_args__ = (
        db.UniqueConstraint('external_user_id', 'benefit', 'currency'),
    )


class Payment(BaseModel):
    __tablename__ = 'payments'
    amount = db.Column(db.Integer, nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False)
